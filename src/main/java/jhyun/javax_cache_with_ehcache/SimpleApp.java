package jhyun.javax_cache_with_ehcache;

import javax.cache.Cache;
import javax.cache.CacheManager;
import javax.cache.Caching;

public class SimpleApp {
    private static final String CACHE_NAME = "test-cache";
    
    public static void main(String[] args) {
        CacheManager cacheManager = Caching.getCacheManager();
        Cache cache = cacheManager.getCache(CACHE_NAME);
        cache.put("test", 42);
        System.out.println(cache.get("test"));
        cacheManager.shutdown();
    }
    
}
